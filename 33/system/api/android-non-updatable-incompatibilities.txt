// Baseline format: 1.0
RemovedClass: android.app.usage.NetworkStatsManager:
    Removed class android.app.usage.NetworkStatsManager
RemovedClass: android.net.NetworkStateSnapshot:
    Removed class android.net.NetworkStateSnapshot
RemovedClass: android.net.NetworkStats:
    Removed class android.net.NetworkStats
RemovedClass: android.net.TrafficStats:
    Removed class android.net.TrafficStats
RemovedClass: android.net.UnderlyingNetworkInfo:
    Removed class android.net.UnderlyingNetworkInfo

RemovedPackage: android.net.netstats.provider:
    Removed package android.net.netstats.provider

RemovedPackage: android.bluetooth:
    Removed package android.bluetooth
RemovedPackage: android.bluetooth.le:
    Removed package android.bluetooth.le

RemovedClass: android.net.IpSecManager:
    Removed class android.net.IpSecManager

RemovedClass: android.net.EthernetManager:
    Removed class android.net.EthernetManager
